#Zach Pedersen

My entry for I/O System Design Tool and MIPS Assembly for CST-307

Download Instructions:
1. Download and Install QTSPIM
2. Download and extract PalindromeChecker file
3. Open QTSPIM and click Load File
4. Select downloaded ASM file
5. Click run and input your palindrome

The approach is to get the character from the beginning and the corresponding character from the end of the string. 
If they match, continue execution until it reaches the midpoint. 
If any pair of characters are found different, stop execution immediately and return "No".